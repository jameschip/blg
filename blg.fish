function blg

    set -g blog_title "This is the blog title"
    set -g blog_index "tmp/index.gmi"
	
	set -g rss_domain "http://your_webs.site"
	set -g rss_title "name of news feed"
	set -g rss_desc "description of news feed"
	set -g rss_index "index.xml"
	
	parse_settings

	echo "RSS title set -> $rss_title"
	echo "RSS description set -> $rss_desc"
	echo "RSS domain set -> $rss_domain"
	echo "RSS index set -> $rss_index"

    argparse 'n/new-post' 'i/init' -- $argv
	
    if set -q _flag_n
        new_post
        return
    end

    if set -q _flag_i
        init_new_blog
        return
    end
    
    build_blog

end

function parse_settings

	if ! test -e blgconf  
		echo "No blgconf"
		return
	end
	
	while read -l line
		switch $line
			case "rsstitle*"
				set -l rsst (string split = $line)
				set -g rss_title $rsst[2]
			case "rssdomain*"
				set -l rssd (string split = $line)
				set -g rss_domain $rssd[2]
			case "rssdesc*"
				set -l rssd (string split = $line)
				set -g rss_desc $rssd[2]
			case "rssindex*"
				set -l rssi (string split = $line)
				set -g rssi $rssi[2]
		end	
	
	end < blgconf
	
end

function init_new_blog

    read -l -P "Name of new blog: " name
    set name (string replace -a ' ' _ $name) 
    mkdir $name
    mkdir "$name/content"
    echo "blog created in $name/"

    cd $name
    create_template
    
    echo "Now let us create your first post!"

    new_post
    
    cd ..

end

function new_post

    if ! test -d content
        echo -e "The content directory for your blog is missing, oh crap."
        return
    end
    read -l -P "Post title: " title 
    set -l post_id (string pad --width 4 --char=0 (math (ls content | wc -l) + 1))
    set -l post_date (date "+%a, %d %b %Y %H:%M:%S %Z")
    echo -e "# $post_id ~ $post_date ~ $title" >> blogmap
    touch content/$post_id.gmi
    echo "-------------------------------------------"
    echo "Created post with id $post_id."
    echo "Edit content/$post_id.html to add content."
    echo "When ready to publish remove the hash from the line in blogmap and run a build."         
end

function build_blog

    set -l article_number ""
    set -l article_date ""
    set -l article_title ""
    
    if ! test -e blogmap
        echo "No blogmap, cant build."
        return
    end 

    if ! test -d content
        echo -e "The content directory for your blog is missing, oh crap."
        return
    end

    if test -d out
        rm -rf out
    end
    mkdir out

    if test -d tmp
        rm -rf tmp
    end
    mkdir tmp

	start_rss    

    while read -l line
        switch $line
            case "#*"
                continue
            case "*"

                set -l parts (string split "~" $line)
                set -l article_number (string trim $parts[1])
                set -l article_date $parts[2]
                set -l article_title $parts[3]

                set -l content_file "content/$article_number.gmi"       # The input 
                set -l article_file "out/$article_number.html"          # The output
                set -l article_link "<a href=\"$article_number.html\">$article_date: $article_title</a><br>" # Link to article
                
                echo "publishing - $article_number $article_title"

                
                set -l article_body (parse $content_file $article_link | string split0)

                set -l tag_tags "<h2>This article is tagged with the following subjects:</h2>"                
                if set -q article_tag_links
                    for item in $article_tag_links;
                        set -a tag_tags "$item<br>"
                    end
                    set -e article_tag_links
                end

                write_file "$article_title" "$article_title" "$tag_tags" "$article_file" "$article_body"
           		add_rss_item "$article_date" "$article_title" "$article_body" "$rss_domain/$article_number.html"
				     
                echo $article_link >> $blog_index
        end


    end < blogmap
	
	end_rss	
	cp $rss_index out/

    echo "Generating index."
    set -l idxbdy (cat $blog_index)
    write_file "Blog index" "Blog index" " " "out/index.html" "$idxbdy"  

     if ! test -e tmp/tagindex
        return
    end 
    
    echo "Generating tags."
    write_tags
    
end

function write_file -a ti de ta of bd
    set -l temp (cat template.html)
    set temp (string replace -a ';;title' $ti $temp)
    set temp (string replace -a ';;description' $de $temp)
    set temp (string replace -a ';;body' $bd $temp)
    set temp (string replace -a ';;title' $ti $temp)
    set temp (string replace -a ';;tags' $ta $temp)
	
	printf "%s\n" $temp >> $of
end

function parse -a input article_link

    set line_number 0
    set code_block 0
    set --global bullets  0 
    
    while read -Lla line
        switch $line
            case "::*" # Put a link in the tag file
                do_tag $line $article_link
                
            case "#*"   # Print a header
                close_bullets
                do_title $line

            case "=>*"  # Print a link
                close_bullets
                print_link $line
                
            case "```*" # Dont print a code block switch but do toggle a variable
                close_bullets
                if test $code_block = 1
                    set code_block 0
                    echo -e "</pre>"
                else
                    set code_block 1
                    echo -e "<pre>"
                end
			case ">*"
				close_bullets
				do_quote $line
            case "*" # handle printing all code to a code block, or printing bullets and paragraphs
                if test $code_block = 1
                    echo -e "$line"
                else
                    if test $line = ""
                        close_bullets
                    else
                        set --local chars (string split -- '' $line)
                        if test $chars[1] = '*'
                            if test $bullets = 0
                                set bullets 1
                                echo -e "<ul>"
                            end
                            echo -e "<li>"(string sub -s 3 $line)"\n</li>"
                        else
                            close_bullets
                            echo -e "<p>$line</p>"
                        end
                    end  
                end
        end
        
    end < $input
    
end

function close_bullets
    if test $bullets = 1
        set bullets 0
        echo -e "</ul>"
    end
end

function print_link -a line
    set line (string sub -s 3 $line)
    set line (string trim $line)
    set --local parts (string split --max 1 -- " " $line)
    echo -e "<a href=\"$parts[1]\">$parts[2]</a>"
end

function do_title -a title_line
    set --local chars (string split '' $title_line)
    for i in (seq (count $chars))
        if test $chars[$i] != '#'
            set --local hnum (math $i - 1)
            set --local title (string sub -s $i $title_line)
            echo "<h$hnum>$title</h$hnum>"
            break
        end
    end
end

function do_quote -a line
	set line (string sub -s 2 $line)
	set line (string trim $line)
	echo "<blockquote>$line</blockquote>"
end

function do_tag -a line article_link
    set tag (string sub -s 3 $line)
    set tag (string trim $tag)
    set sf_tag (string replace -a ' ' _ $tag)    
    set -l tf "out/$sf_tag.html"
    set -l inf "tmp/$sf_tag.gmi"
    set -a -g article_tag_links "<a href=\"$sf_tag.html\">$tag</a>" 

    if ! test -e $inf
        echo "$tag :: $inf :: $tf" >> tmp/tagindex
    end

    echo $article_link >> $inf
end

function write_tags

    while read -l line

        set -l parts (string split "::" $line)
        set title (string trim $parts[1])
        set inf (string trim $parts[2])
        set out (string trim $parts[3])
        set body (cat $inf)
        write_file "$title" "$title" " " "$out" "$body" 
        
    end < tmp/tagindex

end

function create_template

	set template "template.html"

	touch $template

	echo "<!doctype htmml>" >> $template
	echo "<html>"  >> $template
	echo "<head>"  >> $template
	echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"  >> $template 
	echo "<title>;;title</title>"  >> $template
	echo "<meta name=\"description\" content=\";;description\">"  >> $template
	echo "</head>"  >> $template
	echo "<body id=\"top\">"  >> $template
	echo "	<nav>"  >> $template
	echo "		<a href=\"index.html\">index</a>" >> $template
	echo "	</nav>"  >> $template
	echo "	<hr>"  >> $template
	echo "	<main>"  >> $template
	echo "	<h1>;;title</h1>"  >> $template
	echo "	;;body"  >> $template
	echo "	</main>" >> $template
	echo "  <p>;;tags</p>"  >> $template
	echo "  <p><a href=\"#top\">[top]</a></p>"  >> $template
	echo "</body>"  >> $template
	echo "</html>"  >> $template

end

function start_rss
	
	echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" > $rss_index
	echo "<rss version=\"2.0\">" >> $rss_index

	echo "<channel>" >> $rss_index
	echo "<title>$rss_title</title>" >> $rss_index
	echo "<link>$rss_domain</link>" >> $rss_index
	echo "<description>$rss_desc</description>" >> $rss_index

end

function add_rss_item -a date title desc link
	
	echo "    <item>" >> $rss_index
    echo "        <title>$title</title>" >> $rss_index
    echo "        <link>$link</link>" >> $rss_index
	echo "        <pubDate>$date</pubDate>" >> $rss_index
    echo "        <description><![CDATA[$desc]]></description>" >> $rss_index
  	echo "    </item>" >> $rss_index	

end

function end_rss

	echo "</channel>" >> $rss_index
	echo "</rss>" >> $rss_index

end

