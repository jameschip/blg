# blg - a minimal blogging tool.

Blg is a minimalist tool for creating static blogs written, and it lives in a single fish script. 

Blogs are seen sometimes as complex things, requiring large tech stacks and all sorts of fancy esoteric setup to make them work. The truth of the matter is that, at the heart of it, a blog is just a bunch of text files.

This little tool is to show that a blog does not need to be made up of complex databases and dynamically loading pages.

# Install

Clone this repo and ```cp blg.fish ~/.config/fish/functions/blg.fish``` or wherever the fish functions live on your install.

# Creating a new blog
```
blg -i
```

This will create you a new blog, and your first post to go with it!

# Creating a new post

```
blg -n
```

This will create you a new post on your blog. It will tell you witch file to edit to fill it in, and will create a line in the blog map so that you can publish, and unpublish, it at build.

# Building your blog

```
blg
```

Just run blg in the root of your blog and it will create the static output in the ```out/``` directory.

# Publishing posts.

There is a file in the root of your blog called blogmap. Each line of this file is a single entry in your blog. Lines that begin with a "#" are un-published and will be skipped on build, all other lines will produce an output html file.

# About markup in blog posts.

This tool uses gemtext instead of markdown to format blog posts. If you do not know what gemtext is then look it up; it is fast to write and very simple. The only addon to getmtext that this projecct uses is the :: tag.

Lines that begin with :: define a content tag that the article is linked to.

```
:: games
:: writing
```

The two lines above tag an article as being in the games, and writing categories. All articles with similar tags are collected together into an html file that allows you to see all the other articles taged as such.

# The template.html file

The static output is created from a template.html. This file contains spectial tags that are then replaced during build to give you a nicely formatted blog at the end. Look at the one in this repo for an example.

# The blgconf file

You can create a file in the root of your blog named blgconf, it is used to change settings while building your blog. The following can be set:

rssindex - the name of the rss file.
rssdomain - the domain where the rss file will sit, e.g. http://youordomain.com the rss feed will be at http://yourdomain.com/index.xml
rssdesc - the description of the rss feed.
rsstitle - the title of the rss feed.

One setting per line, syntax as follows:

rssindex=index.xml
rsstitle=RSS title
rssdesc=RSS description
rssdomain=http://yourdomain.com



# Should I use this?

Go ahead, although I make no claim to how stable or useful it is. I built this for fun, and if you find use for it then good, but it isn't intended for every day use.
